import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'studentPage.dart';
import 'gradingPage.dart';
import 'costPage.dart';
import 'enrollPage.dart';
import 'loginPage.dart';
import 'homePage.dart';
import 'schedulePage.dart';

void main() {
  runApp(DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          locale: DevicePreview.locale(context),
          theme: ThemeData(
            primarySwatch: Colors.yellow,
          ),
          home: MainApp())));
}

class MainApp extends StatefulWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  final List<Widget> _listPage = [
    HomePage(),
    SchedulePage(),
    CostPage(),
    StudentPage(),
    GradingPage(),
    EnrollPage(),
    LoginPage(),
  ];
  int _selectedListPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BURAPHA UNIVERSITY"),
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  _selectedListPage = 6;
                });
              },
              icon: Icon(Icons.login))
        ],
      ),
      body: _listPage[_selectedListPage],
      drawer: Drawer(
        backgroundColor: Colors.yellow[300],
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/buu-logo11.png'),
                ),
                color: Colors.grey,
              ),
              child: Text(''),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: const Text('หน้าแรก'),
              onTap: () {
                setState(() {
                  _selectedListPage = 0;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.calendar_month),
              title: const Text('ตารางเรียน'),
              onTap: () {
                setState(() {
                  _selectedListPage = 1;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.attach_money),
              title: const Text('ภาระค่าใช้จ่ายทุน'),
              onTap: () {
                setState(() {
                  _selectedListPage = 2;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: const Text('ประวัตินิสิต'),
              onTap: () {
                setState(() {
                  _selectedListPage = 3;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.grading),
              title: const Text('ผลการศึกษา'),
              onTap: () {
                setState(() {
                  _selectedListPage = 4;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.receipt_long),
              title: const Text('การลงทะเบียน'),
              onTap: () {
                setState(() {
                  _selectedListPage = 5;
                });
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
