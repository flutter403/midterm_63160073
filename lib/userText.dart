import 'package:flutter/material.dart';

class UserName extends StatelessWidget {
  const UserName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Align(
        alignment: Alignment.topLeft,
        child: Text("63160073 : นายธนดล บุญถึง : คณะวิทยาการสารสนเทศ\n"
            "หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -\n"
            "สถานภาพ: กำลังศึกษา\n"
            "อ. ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.ใครไม่รู้ เหมือนกัน"),
      ),
    );
  }
}
