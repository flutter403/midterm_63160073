import 'package:flutter/material.dart';

import 'userText.dart';

class CostPage extends StatelessWidget {
  const CostPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          child: Column(
            children: [
              Row(
                children: [
                  UserName(),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      "ภาระค่าใช้จ่าย ประจำปีการศึกษา 2565/2",
                      style: TextStyle(color: Colors.orange, fontSize: 20),
                    )),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      "วันที่ 14/11/2022",
                    )),
                  ],
                ),
              ),
              Table(
                columnWidths: {
                  0: FlexColumnWidth(1),
                  1: FlexColumnWidth(1),
                  2: FlexColumnWidth(1),
                  3: FlexColumnWidth(1),
                  4: FlexColumnWidth(1),
                },
                border: TableBorder.all(),
                children: [
                  TableRow(children: [
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "รายการ",
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.amber,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "จำนวนเงิน",
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.amber,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "เลขที่ใบเสร็จ",
                        style: TextStyle(fontSize: 12.5),
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.amber,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "วันผ่อนชำระ",
                        style: TextStyle(fontSize: 12.5),
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.amber,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "หมายเหตุ",
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.amber,
                    ),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "99999901-59",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "600.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "99999902-59",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "600.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "99999903-59",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "600.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "99999904-59",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "600.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "99999905-59",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "600.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "ค่าบำรุงมหาวิทยาลัย",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "1,000.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "ค่าบำรุงหอสมุด",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "700.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "ค่าบำรุงคณะ",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "15,300.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                  TableRow(children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "ค่าธรรมเนียม",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "20.00",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                  ]),
                ],
              ),
              Table(
                columnWidths: {
                  0: FlexColumnWidth(1),
                  1: FlexColumnWidth(1),
                  2: FlexColumnWidth(3),
                },
                border: TableBorder.all(),
                children: [
                  TableRow(children: [
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "รวม",
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.lightBlueAccent,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "20,020.00",
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.lightBlueAccent,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "",
                        textAlign: TextAlign.center,
                      ),
                      color: Colors.lightBlueAccent,
                    ),
                  ]),
                ],
              ),
            ],
          ),
          margin: EdgeInsets.all(20.0),
        ),
      ],
    );
  }
}
