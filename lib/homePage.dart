import 'package:flutter/material.dart';

import 'userText.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          child: Column(
            children: [
              Row(
                children: [
                  UserName(),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              Container(
                color: Colors.grey,
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      "ประกาศ",
                      style: TextStyle(color: Colors.white),
                    ))
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(child: Text("1. การทำบัตรนิสิตกับธนาคารกรุงไทย"))
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: Text(
                    "   กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท\n"
                    "   สำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา \n"
                    "   ส่วนนิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง",
                    style: TextStyle(color: Colors.red),
                  ))
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: Align(
                    alignment: Alignment.topLeft,
                    child: Image(image: AssetImage('assets/card1_1.jpg')),
                  ))
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: Align(
                    alignment: Alignment.topLeft,
                    child: Image(image: AssetImage('assets/card1_2.png')),
                  ))
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: Align(
                    alignment: Alignment.topLeft,
                    child: Image(image: AssetImage('assets/card1_3.png')),
                  ))
                ],
              )
            ],
          ),
          margin: EdgeInsets.all(20.0),
        ),
      ],
    );
  }
}
