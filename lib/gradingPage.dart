import 'package:flutter/material.dart';

import 'userText.dart';

class GradingPage extends StatelessWidget {
  const GradingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.all(20.0),
          alignment: Alignment.center,
          child: Column(
            children: [
              Row(
                children: [
                  UserName(),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      color: Colors.cyan,
                      child: Text(
                        "ผลการศึกษา(ทุกภาคเรียน)",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                      width: double.infinity,
                    ),
                    Container(
                      color: Colors.grey,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "C.Register\n24",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "C.Earn\n24",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "CA\n24",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "GP\n197.5",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "GPA\n3.66",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                      width: double.infinity,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            color: Colors.amber,
                            child: Text(
                              "ภาคเรียน 1/2565",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                            width: double.infinity,
                          ),
                          Container(
                            color: Colors.grey[300],
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "รหัสวิชา\n88880159\n88880259\n88880359",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "วิชา\nXXXXXXXXXX\nXXXXXXXXXX\nXXXXXXXXXX",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "หน่วยกิต\n3\n3\n2",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "เกรด\nB\nA\nB+",
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                            width: double.infinity,
                          ),
                          Container(
                            color: Colors.grey,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "C.Register\n24",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "C.Earn\n24",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "CA\n24",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "GP\n197.5",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "GPA\n3.66",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),
                            width: double.infinity,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            color: Colors.amber,
                            child: Text(
                              "ภาคเรียน 2/2564",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                            width: double.infinity,
                          ),
                          Container(
                            color: Colors.grey[300],
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "รหัสวิชา\n77770159\n77770259\n77770359",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "วิชา\nXXXXXXXXXX\nXXXXXXXXXX\nXXXXXXXXXX",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "หน่วยกิต\n3\n3\n2",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "เกรด\nB\nA\nB+",
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                            width: double.infinity,
                          ),
                          Container(
                            color: Colors.grey,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "C.Register\n16",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "C.Earn\n16",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "CA\n16",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "GP\n197.5",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "GPA\n3.66",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),
                            width: double.infinity,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            color: Colors.amber,
                            child: Text(
                              "ภาคเรียน 1/2564",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                            width: double.infinity,
                          ),
                          Container(
                            color: Colors.grey[300],
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "รหัสวิชา\n25710259\n30211359\n99910159",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "วิชา\nXXXXXXXXXX\nXXXXXXXXXX\nXXXXXXXXXX",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "หน่วยกิต\n3\n3\n2",
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "เกรด\nB\nA\nB+",
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                            width: double.infinity,
                          ),
                          Container(
                            color: Colors.grey,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  "C.Register\n8",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "C.Earn\n8",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "CA\n8",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "GP\n197.5",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "GPA\n3.66",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),
                            width: double.infinity,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
