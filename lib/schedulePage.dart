import 'package:flutter/material.dart';

import 'userText.dart';

class SchedulePage extends StatefulWidget {
  const SchedulePage({Key? key}) : super(key: key);

  @override
  _SchedulePage createState() => _SchedulePage();
}

class _SchedulePage extends State<SchedulePage> {
  @override
  Widget build(BuildContext context) {
    final maxWidth = MediaQuery.of(context).size.width;
    final scaleWidth = maxWidth / 390;
    final maxHeight = MediaQuery.of(context).size.height;
    final scaleHeight = maxHeight / 844;

    final List<Widget> list = [
      LearnSchedule(),
      MidtermSchedule(),
      FinalSchedule(),
    ];
    int _index = 0;
    return ListView(
      children: [
        Container(
          child: Column(children: [
            Row(
              children: [
                UserName(),
              ],
            ),
            Divider(
              color: Colors.orange,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _index = 0;
                      });
                    },
                    child: Text("ตารางเรียน")),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _index = 1;
                      });
                    },
                    child: Text("สอบกลางภาค")),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _index = 2;
                      });
                    },
                    child: Text("สอบปลายภาค")),
              ],
            ),
            Divider(
              color: Colors.orange,
            ),
            list[_index],
          ]),
          margin: EdgeInsets.all(20.0),
        ),
      ],
    );
  }
}

Widget LearnSchedule() {
  return Container(
    child: Column(
      children: [
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Text(
                "ตารางเรียน",
                style: TextStyle(color: Colors.orange, fontSize: 20),
              )),
            ],
          ),
        ),
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Text(
                "ปีการศึกษา 2565 / 2 ",
              )),
            ],
          ),
        ),
        Table(
          columnWidths: {
            0: FixedColumnWidth(50),
            1: FlexColumnWidth(1),
            2: FlexColumnWidth(1),
            3: FlexColumnWidth(1),
            4: FlexColumnWidth(1),
            5: FlexColumnWidth(1),
            6: FlexColumnWidth(1)
          },
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "วัน/เวลา",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
                color: Colors.grey,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "10:00-11:00",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                color: Colors.grey,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "11:00-12:00",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                color: Colors.grey,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "12:00-13:00",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                color: Colors.grey,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "13:00-14:00",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                color: Colors.grey,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "14:00-15:00",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                color: Colors.grey,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "15:00-16:00",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                color: Colors.grey,
              ),
            ]),
          ],
        ),
        Table(
          columnWidths: {
            0: FixedColumnWidth(50),
            1: FlexColumnWidth(2),
            2: FlexColumnWidth(1),
            3: FlexColumnWidth(2),
            4: FlexColumnWidth(1),
          },
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "จันทร์",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999901-59\nIF-4M210",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999902-59\nIF-3M210",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "อังคาร",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999903-59\nIF-4C02",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999901-59\nIF-3C03",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
            ]),
          ],
        ),
        Table(
          columnWidths: {
            0: FixedColumnWidth(50),
            1: FlexColumnWidth(2),
            2: FlexColumnWidth(2),
            3: FlexColumnWidth(2),
          },
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "พุธ",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999902-59\nIF-4C02",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999903-59\nIF-3C01",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
            ]),
          ],
        ),
        Table(
          columnWidths: {
            0: FixedColumnWidth(50),
            1: FlexColumnWidth(6),
          },
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "พฤหัส",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "\n",
                    style: TextStyle(fontSize: 12),
                  )),
            ]),
          ],
        ),
        Table(
          columnWidths: {
            0: FixedColumnWidth(50),
            1: FlexColumnWidth(2),
            2: FlexColumnWidth(1),
            3: FlexColumnWidth(2),
            4: FlexColumnWidth(1),
          },
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "ศุกร์",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999904-59\nIF-5T05",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "99999905-59\nIF-4C02",
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightGreenAccent,
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "",
                    style: TextStyle(fontSize: 12),
                  )),
            ]),
          ],
        ),
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Text(
                "* ข้อมูลในตารางประกอบด้วย รหัสวิชา และห้องเรียน",
              )),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget MidtermSchedule() {
  return Container(
    child: Column(
      children: [
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Text(
                "สอบกลางภาค",
                style: TextStyle(color: Colors.orange, fontSize: 20),
              )),
            ],
          ),
        ),
        Table(
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "รหัสวิชา",
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightBlueAccent,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "ชื่อวิชา",
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightBlueAccent,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "กลุ่ม",
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightBlueAccent,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "วัน/เวลา",
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightBlueAccent,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "ห้องสอบ",
                  textAlign: TextAlign.center,
                ),
                color: Colors.lightBlueAccent,
              ),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999902-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "1",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "20/01/23\n10:00-12:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-4C02",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999904-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "3",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "22/01/23\n17:00-19:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-4C01",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999905-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "1",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "23/01/23\n13:00-16:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-3M280",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
          ],
        ),
      ],
    ),
  );
}

Widget FinalSchedule() {
  return Container(
    child: Column(
      children: [
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Text(
                "สอบปลายภาค",
                style: TextStyle(color: Colors.orange, fontSize: 20),
              )),
            ],
          ),
        ),
        Table(
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "รหัสวิชา",
                  textAlign: TextAlign.center,
                ),
                color: Colors.cyan,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "ชื่อวิชา",
                  textAlign: TextAlign.center,
                ),
                color: Colors.cyan,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "กลุ่ม",
                  textAlign: TextAlign.center,
                ),
                color: Colors.cyan,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "วัน/เวลา",
                  textAlign: TextAlign.center,
                ),
                color: Colors.cyan,
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  "ห้องสอบ",
                  textAlign: TextAlign.center,
                ),
                color: Colors.cyan,
              ),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999901-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "1",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "20/04/23\n10:00-12:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-3C02",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999902-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "1",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "20/04/23\n13:00-16:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-4C02",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999903-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "2",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "21/04/23\n10:00-12:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-4M280",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999904-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "3",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "22/01/23\n17:00-19:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-4C01",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
            TableRow(children: [
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "99999905-59",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "XXXXXXXXXX",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "1",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "23/01/23\n13:00-16:00",
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "IF-3M280",
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  )),
            ]),
          ],
        ),
      ],
    ),
  );
}
