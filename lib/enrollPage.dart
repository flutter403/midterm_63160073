import 'package:flutter/material.dart';

import 'userText.dart';

class EnrollPage extends StatefulWidget {
  const EnrollPage({Key? key}) : super(key: key);

  @override
  _EnrollPage createState() => _EnrollPage();
}

class _EnrollPage extends State<EnrollPage> {
  final List<Widget> list = [
    ResultEnroll(),
    Enroll(),
  ];
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.all(20.0),
          alignment: Alignment.center,
          child: Column(
            children: [
              Row(
                children: [
                  UserName(),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _index = 0;
                        });
                      },
                      child: Text("ผลการลงทะเบียน")),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _index = 1;
                        });
                      },
                      child: Text("ลงทะเบียน")),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              list[_index],
            ],
          ),
        ),
      ],
    );
  }
}

Widget Enroll() {
  return Container(
      child: Column(
    children: [
      Container(
        child: Row(
          children: [
            Expanded(
                child: Text(
              "ลงทะเบียน",
              style: TextStyle(color: Colors.orange, fontSize: 20),
            )),
          ],
        ),
      ),
      Text(
        "ยังไม่ได้อยู่ในช่วงการลงทะเบียน",
        style: TextStyle(color: Colors.red),
      ),
    ],
  ));
}

Widget ResultEnroll() {
  return Container(
    child: Column(
      children: [
        Container(
          child: Row(
            children: [
              Expanded(
                  child: Text(
                "ผลการลงทะเบียน",
                style: TextStyle(color: Colors.orange, fontSize: 20),
              )),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            children: [
              Container(
                color: Colors.cyan,
                child: Text(
                  "รายชื่อวิชาที่ลงทะเบียนทั้งหมด",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                width: double.infinity,
              ),
              Container(
                color: Colors.grey[300],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "รหัสวิชา",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "ชื่อวิชา",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "หน่วยกิต",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "เกรด",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "77770159",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "XXXXXXXXX",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "3",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "77770259",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "XXXXXXXXX",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "3",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "77770359",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "XXXXXXXXX",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "3",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "77770459",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "XXXXXXXXX",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "3",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "77770559",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "XXXXXXXXX",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "2",
                          textAlign: TextAlign.center,
                        )),
                        Expanded(
                            child: Text(
                          "",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                          "จำนวนหน่วยกิตรวม 14",
                          textAlign: TextAlign.center,
                        )),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}
