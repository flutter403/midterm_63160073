import 'package:flutter/material.dart';

import 'userText.dart';

class StudentPage extends StatefulWidget {
  const StudentPage({Key? key}) : super(key: key);

  @override
  _StudentPage createState() => _StudentPage();
}

class _StudentPage extends State<StudentPage> {
  final List<Widget> list = [
    LearningInfo(),
    PersonInfo(),
  ];
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.all(20.0),
          alignment: Alignment.center,
          child: Column(
            children: [
              Row(
                children: [
                  UserName(),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _index = 0;
                        });
                      },
                      child: Text("ช้อมูลการศึกษา")),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _index = 1;
                        });
                      },
                      child: Text("ช้อมูลส่วนบุคคล")),
                ],
              ),
              Divider(
                color: Colors.orange,
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Text(
                      "ระเบียนประวัติ",
                      style: TextStyle(color: Colors.orange, fontSize: 20),
                    )),
                  ],
                ),
              ),
              list[_index],
            ],
          ),
        ),
      ],
    );
  }
}

Widget LearningInfo() {
  return Container(
    child: Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            children: [
              Container(
                color: Colors.cyan,
                child: Text(
                  "ข้อมูลด้านการศึกษา",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                width: double.infinity,
              ),
              Container(
                color: Colors.grey[300],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Expanded(child: Text("รหัสประจำตัว:")),
                        Expanded(child: Text("63160073")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("เลขที่บัตรประชาชน:")),
                        Expanded(child: Text("1234567890000")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ชื่อ:")),
                        Expanded(child: Text("นายธนดล บุญถึง")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ชื่ออังกฤษ:")),
                        Expanded(child: Text("MR. TANADON BOONTONG")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("คณะ:")),
                        Expanded(child: Text("คณะวิทยาการสารสนเทศ")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("วิทยาเขต:")),
                        Expanded(child: Text("บางแสน")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("หลักสูตร:\n\n")),
                        Expanded(
                            child: Text(
                                "2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ปีการศึกษาที่เข้า:\n")),
                        Expanded(child: Text("2563/1\nวันที่ 29/4/2563 ")),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}

Widget PersonInfo() {
  return Container(
    child: Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Container(
                color: Colors.cyan,
                child: Text(
                  "ข้อมูลส่วนบุคคล",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                width: double.infinity,
              ),
              Container(
                color: Colors.grey[300],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Expanded(child: Text("สัญชาติ:")),
                        Expanded(child: Text("ไทย")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ศาสนา:")),
                        Expanded(child: Text("พุทธ")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ชื่อบิดา:")),
                        Expanded(child: Text("นายไม่ ระบุ")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ชื่อมารดา:")),
                        Expanded(child: Text("นางสาวไม่ ระบุ")),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(child: Text("ที่อยู่:\n\n\n\n\n\n ")),
                        Expanded(
                            child: Text(
                                "999/9\n ตำบล: วังม่วง\n เขต/อำเภอ: วังม่วง\n จังหวัด: สระบุรี\n 18220\n โทร: 0638844262")),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}
