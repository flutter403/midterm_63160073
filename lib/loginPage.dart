import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final maxWidth = MediaQuery.of(context).size.width;
    final scaleWidth = maxWidth / 390;
    final maxHeight = MediaQuery.of(context).size.height;
    final scaleHeight = maxHeight / 844;
    return Scaffold(
      body: OrientationBuilder(builder: (_, orientation) {
        if (orientation == Orientation.portrait)
          return PortraitLogin(scaleHeight, scaleWidth);
        else
          return LandscapeLogin(scaleHeight, scaleWidth);
      }),
    );
  }
}

Widget PortraitLogin(double scaleHeight, double scaleWidth) {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Spacer(flex: 5),
        ConstrainedBox(
          constraints: BoxConstraints(
              maxHeight: 150 * scaleHeight, maxWidth: 150 * scaleWidth),
          child: Row(
            children: [
              Image(
                image: AssetImage('assets/buu-logo11.png'),
                width: 149 * scaleWidth,
                height: 149 * scaleHeight,
              ),
            ],
          ),
        ),
        const Spacer(flex: 5),
        Center(
          child: Container(
            width: 300 * scaleWidth,
            height: 150 * scaleHeight,
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(labelText: 'รหัสนิสิต'),
                  style: TextStyle(fontSize: 16 * scaleHeight),
                ),
                TextField(
                  decoration: InputDecoration(labelText: 'รหัสผ่าน'),
                  style: TextStyle(fontSize: 16 * scaleHeight),
                ),
              ],
            ),
          ),
        ),
        const Spacer(flex: 5),
        Center(
          child: Container(
            width: 150 * scaleWidth,
            height: 50 * scaleHeight,
            child: ElevatedButton(
                onPressed: () {},
                child: Text(
                  'เข้าสู่ระบบ',
                  style: TextStyle(fontSize: 16 * scaleHeight),
                )),
          ),
        ),
        const Spacer(flex: 10),
      ],
    ),
    margin: EdgeInsets.all(20.0),
  );
}

Widget LandscapeLogin(double scaleHeight, double scaleWidth) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            const Spacer(flex: 5),
            ConstrainedBox(
              constraints: BoxConstraints(
                  maxHeight: 250 * scaleHeight, maxWidth: 150 * scaleWidth),
              child: Row(
                children: [
                  Image(
                    image: AssetImage('assets/buu-logo11.png'),
                    width: 149 * scaleWidth,
                    height: 249 * scaleHeight,
                  ),
                ],
              ),
            ),
            const Spacer(flex: 5)
          ],
        ),
        Column(
          children: [
            const Spacer(flex: 5),
            Center(
              child: Container(
                width: 100 * scaleWidth,
                height: 150 * scaleHeight,
                child: TextField(
                  decoration: InputDecoration(labelText: 'รหัสนิสิต'),
                  style: TextStyle(fontSize: 16 * scaleHeight),
                ),
              ),
            ),
            Center(
              child: Container(
                width: 100 * scaleWidth,
                height: 150 * scaleHeight,
                child: TextField(
                  decoration: InputDecoration(labelText: 'รหัสผ่าน'),
                  style: TextStyle(fontSize: 16 * scaleHeight),
                ),
              ),
            ),
            const Spacer(flex: 2),
            Center(
              child: Container(
                width: 80 * scaleWidth,
                height: 50 * scaleHeight,
                child: ElevatedButton(
                    onPressed: () {},
                    child: Text(
                      'เข้าสู่ระบบ',
                      style: TextStyle(fontSize: 16 * scaleHeight),
                    )),
              ),
            ),
            const Spacer(flex: 3),
          ],
        ),
      ],
    ),
    margin: EdgeInsets.all(20.0),
  );
}
